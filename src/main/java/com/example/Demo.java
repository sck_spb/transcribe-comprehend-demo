package com.example;

import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.services.comprehend.ComprehendClient;
import software.amazon.awssdk.services.comprehend.model.*;
import software.amazon.awssdk.services.transcribestreaming.TranscribeStreamingAsyncClient;
import software.amazon.awssdk.services.transcribestreaming.model.*;
import software.amazon.awssdk.services.transcribestreaming.model.LanguageCode;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.TargetDataLine;
import java.util.List;

public class Demo {

    private static ComprehendClient comprehend;

    public static void main(String[] args) throws Exception {
        ProfileCredentialsProvider awsCredentialsProvider = ProfileCredentialsProvider.create();
        setUpComprehend(awsCredentialsProvider);

        TranscribeStreamingAsyncClient client =
                TranscribeStreamingAsyncClient.builder()
                        .credentialsProvider(awsCredentialsProvider).build();

        StartStreamTranscriptionRequest request = StartStreamTranscriptionRequest.builder()
                .mediaEncoding(MediaEncoding.PCM)
                .languageCode(LanguageCode.EN_US)
                .mediaSampleRateHertz(16_000).build();

        TargetDataLine mic = Microphone.init();

        AudioStreamPublisher publisher = new AudioStreamPublisher(new AudioInputStream(mic));

        StartStreamTranscriptionResponseHandler response =
                StartStreamTranscriptionResponseHandler.builder().subscriber(e -> {
                    TranscriptEvent event = (TranscriptEvent) e;
                    List<Result> results = event.transcript().results();
                    for (Result r : results) {
                        if (r.isPartial()) continue;
                        for (Alternative a : r.alternatives()) {
                            DetectSentimentResponse detectSentimentResponse = comprehend.detectSentiment(DetectSentimentRequest.builder()
                                    .languageCode(software.amazon.awssdk.services.comprehend.model.LanguageCode.EN)
                                    .text(a.transcript())
                                    .build());
                            DetectEntitiesResponse detectEntitiesResponse = comprehend.detectEntities(DetectEntitiesRequest.builder()
                                    .languageCode(software.amazon.awssdk.services.comprehend.model.LanguageCode.EN)
                                    .text(a.transcript())
                                    .build());
                            DetectKeyPhrasesResponse detectKeyPhrasesResponse = comprehend.detectKeyPhrases(DetectKeyPhrasesRequest.builder()
                                    .languageCode(software.amazon.awssdk.services.comprehend.model.LanguageCode.EN)
                                    .text(a.transcript()).build());
                            DetectDominantLanguageResponse detectDominantLanguageResponse = comprehend.detectDominantLanguage(DetectDominantLanguageRequest.builder()
                                    .text(a.transcript())
                                    .build());
                            DetectSyntaxResponse detectSyntaxResponse = comprehend.detectSyntax(DetectSyntaxRequest.builder()
                                    .languageCode(SyntaxLanguageCode.EN)
                                    .text(a.transcript())
                                    .build());
                            System.out.println("Transcription result: [" + a.transcript() + "]" +
                                    "\n\t\t\t with detected language " + detectDominantLanguageResponse +
                                    "\n\t\t\t with sentiment " + detectSentimentResponse +
                                    "\n\t\t\t with entities" + detectEntitiesResponse +
                                    "\n\t\t\t with key phrases " + detectKeyPhrasesResponse +
                                    "\n\t\t\t with syntax " + detectSyntaxResponse);
                        }
                    }
                }).build();

        client.startStreamTranscription(request, publisher, response).join();
    }

    public static void setUpComprehend(ProfileCredentialsProvider awsCredentialsProvider) {
        comprehend = ComprehendClient.builder()
                .credentialsProvider(awsCredentialsProvider)
                .build();
    }
}
